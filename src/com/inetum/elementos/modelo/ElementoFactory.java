package com.inetum.elementos.modelo;

import java.util.ArrayList;
import java.util.List;

public abstract class ElementoFactory {
	protected static final int PIEDRA = 0;
	protected static final int PAPEL = 1;
	protected static final int TIJERA = 2;
	protected String elementoFactory;
	private static List<ElementoFactory> elementos;
	protected String nombre;
	protected int numero;
	protected String descripcionResultado;

	// constructor
	public ElementoFactory(String nombre, int numero) {
		this.nombre = nombre;
		this.numero = numero;
	}

	// getters / setters
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getElementoFactory() {
		return elementoFactory;
	}

	public String getDescripcionResultado() {
		return descripcionResultado;
	}

	// métodos de negocio
	public boolean isMe(int pNum) {
		return numero == pNum;
	}

	public abstract int comparar(ElementoFactory pElem);

	public static ElementoFactory getInstance(int pNum) {
		elementos = new ArrayList<ElementoFactory>();
		elementos.add(new Piedra());
		elementos.add(new Papel());
		elementos.add(new Tijera());

		// van a ir el resto de los elementos
		for (ElementoFactory elementoFactory : elementos) {
			if (elementoFactory.isMe(pNum))
				return elementoFactory;

		}
		return null;
	}
}
