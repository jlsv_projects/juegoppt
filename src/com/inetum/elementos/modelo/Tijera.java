package com.inetum.elementos.modelo;

public class Tijera extends ElementoFactory {

	public Tijera() {
		super("Tijera", 2);
	}

	@Override
	public int comparar(ElementoFactory pElem) {
		int numero = pElem.getNumero();
	    int resultado =0;

	    if(numero == PAPEL) {
	    	resultado =1;
	    	descripcionResultado = "tijera le gano a papel";
	    }
	    	
	    else if (numero ==  PIEDRA) {
	    	resultado = -1;
	    	descripcionResultado = "tijera perdio con piedra";
	    }
	    	
	    else {
	    	resultado = 0;
	    	descripcionResultado = "EMPATE";
	    }
	    	
	    
		return resultado;
	}

}
