package com.inetum.elementos.modelo.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.inetum.elementos.modelo.ElementoFactory;
import com.inetum.elementos.modelo.Papel;
import com.inetum.elementos.modelo.Piedra;
import com.inetum.elementos.modelo.Tijera;

public class ElementoFactoryTest {

	// lote de pruebas
	Piedra piedra;
	Papel papel;
	Tijera tijera;

	@Before
	public void setUp() throws Exception {
		// antes de cada test
		piedra = new Piedra();
		papel = new Papel();
		tijera = new Tijera();
	}

	@After
	public void tearDown() throws Exception {
		// despues de cada test
		piedra = null;
		papel = null;
		tijera = null;
	}

	@Test
	public void testCompararPiedraConTijera() {
		assertEquals(1, piedra.comparar(tijera));
		assertEquals("Piedra le gano a Tijera", piedra.getDescripcionResultado());
	}

	@Test
	public void testCompararPiedraConPapel() {
		assertEquals(-1, piedra.comparar(papel));
	}

	@Test
	public void testCompararPiedraConPiedra() {
		assertEquals(0, piedra.comparar(piedra));
	}
	
	@Test
	public void testCompararPapelConTijera() {
		assertEquals(-1, papel.comparar(tijera));
		assertEquals("papel perdio con tijera", papel.getDescripcionResultado());
	}

	@Test
	public void testCompararPapelConPapel() {
		assertEquals(0, papel.comparar(papel));
	}

	@Test
	public void testCompararPapelConPiedra() {
		assertEquals(1, papel.comparar(piedra));
	}
	
	@Test
	public void testCompararTijeraConTijera() {
		assertEquals(0, tijera.comparar(tijera));
		assertEquals("EMPATE", tijera.getDescripcionResultado());
	}

	@Test
	public void testCompararTijeraConPapel() {
		assertEquals(1, tijera.comparar(papel));
	}

	@Test
	public void testCompararTijeraConPiedra() {
		assertEquals(-1, tijera.comparar(piedra));
	}

	@Test
	public void testGetInstancePiedra() {
		assertTrue(ElementoFactory.getInstance(0) instanceof Piedra);
	}

	@Test
	public void testGetInstancePapel() {
		assertTrue(ElementoFactory.getInstance(1) instanceof Papel);
	}

	@Test
	public void testGetInstanceTijera() {
		assertTrue(ElementoFactory.getInstance(2) instanceof Tijera);
	}

}
